---
title: Introduction
sidebar_position: 0
slug: /
---

Elm Story is a free, no-code tool that helps authors, designers and students visually develop and publish interactive stories.

:::info
Elm Story now supports game compilation.

Read the [devlog](https://elmstorygames.itch.io/elm-story/devlog/307895/elm-story-05-early-access-launch) for more information on this pivotal early access release.
:::

:::caution
Documentation is under active development and is rapidly changing and incomplete.

The 0.6.0 release has a number of term changes that are not fully reflected.

If you have questions, please reach out on [r/ElmStoryGames](https://reddit.com/r/ElmStoryGames/) or join our [community Discord server](https://discord.gg/v897evyc4Q).
:::

## Platform Overview

Elm Story consists of 3 progressive layers. The first layer is production through the Elm Story app; a modern, cross-platform desktop application for Windows, Linux and macOS.

![Example banner](/img/introduction/intro-slide.png)

### Production

Elm Story provides a dashboard for organizing your games, a visual editor for design and a hot-reload-supporting runtime engine (ESRE) for testing as you go.

### Data

Elm Story doesn't have a save button, instead opting to auto-save your progress to an internal database. You can import and export your games as portable data using the JSON format. This data is readable by Elm Story and 3rd party tools for use in existing pipelines that may include game engines like Godot, Unreal and Unity.

### Publication

Elm Story supports game export to PWA; an offline-ready, installable web app that works great on both desktop and mobile browsers. For hosting, use your own web server or free, 3rd party service like Neocities, Vercel, Netlify or Firebase.

With Cordova or Electron, wrap your game in an app ready for iOS, Android, Windows and macOS app stores.

:::info
In 2022, Elm Story will expand to support direct export to hosting so that your games are made available on the Web and native mobile app, without having to jump through self-hosting technical hoops.
:::
