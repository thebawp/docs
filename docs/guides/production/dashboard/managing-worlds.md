---
title: Managing Storyworlds
sidebar_position: 2
pagination_next: guides/production/composer/composer-overview
---

## Create Game

1. Click create.

![Create Game](/img/production/dashboard/create-game.png)

2. Enter game title and designer and click save.

![New Game Modal](/img/production/dashboard/new-game-modal.png)

Once your studio has at least 1 game, the create game button will be the last box in the library.

![Game Created](/img/production/dashboard/game-created.png)

:::info
Games are currently ordered by last opened.

Future releases will add additional sort and display options.
:::

## Remove Game

1. Click the remove storyworld button in the bottom-left corner of the game box.

![Remove Game](/img/production/dashboard/remove-game.png)

2. Click the remove storyworld button in the modal to confirm.

![Confirm Remove Game](/img/production/dashboard/confirm-remove-game.png)

## Edit Game

1. Click the game box to open the editor.

![Edit Game](/img/production/dashboard/edit-game.png)

## Import Game

Click [here](importing-worlds.md) for a guide to importing games to Elm Story.
