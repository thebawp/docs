---
title: Managing Studios
sidebar_position: 1
---

It's common for designers to contribute to different organizations.

Studios accommodate this reality by keeping disparate game libraries organized.

:::info
Future versions of Elm Story will leverage the Studio convention to enable collaboration, expand [quality-based narrative](https://emshort.blog/2016/04/12/beyond-branching-quality-based-and-salience-based-narrative-structures/) support and more.
:::

## Create Studio

1. Click the studio dropdown > create studio button.

![Create Studio](/img/production/dashboard/create-studio.png)

2. Name your studio and click save.

![New Studio Modal](/img/production/dashboard/new-studio-modal.png)

## Select Studio

By default, Elm Story will automatically select your newly created studio.

![Selected Studio](/img/production/dashboard/selected-studio.png)

1. Select a different studio by clicking the studio dropdown > desired studio.

![Select Studio](/img/production/dashboard/select-studio.png)

## Edit / Remove Studio

1. With the desired studio selected, click the edit studio button.

![Edit Studio](/img/production/dashboard/edit-studio.png)

2. To edit, make name adjustments and click save. To remove studio, click remove.

![Edit / Remove Studio](/img/production/dashboard/edit-remove-studio-modal.png)

:::danger
Removing a studio will remove all associated studio games from the internal database.

Make sure to export games you wish to save outside of Elm Story.
:::
