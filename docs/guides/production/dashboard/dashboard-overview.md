---
title: Dashboard Overview
pagination_label: Dashboard Overview
sidebar_label: Overview
sidebar_position: 0
slug: overview
---

Elm Story's dashboard is used to access [game libraries](../managing-worlds) nested under representative [studios](../managing-studios).

![Dashboard Overview](/img/production/dashboard/dashboard-overview.png)
