---
title: Importing Storyworlds
sidebar_position: 3
---

Elm Story supports import of the Elm Story game format: portable JSON data based on our [open schema](https://www.npmjs.com/package/@elmstorygames/schemas).

## Import

1. In the dashboard, click the import game button.

![Import Game Button](/img/production/dashboard/import-game-button.png)

2. Select the game `.json` in the file explorer and click open.

:::info
Elm Story will append `(Imported)` to the end of the imported game title. Open the game in the editor to change the title.
:::

## Versioning

Games exported from Elm Story includes engine and schema version data.

Starting with `0.1.3`, when importing games exported from older versions, Elm Story will attempt to upgrade the imported game to the installed engine features.

:::caution
Elm Story no longer supports game data <= `0.1.2`.
:::

The original `.json` file will not be modified, but future exports will use the installed engine and schema versions.

## Conflicts

### Studio Doesn't Exist

Games exported from Elm Story includes data about the parent studio.

If you import a game to an installation of Elm Story, and the studio does not exist, you will be asked to confirm studio creation before the import can complete.

![Import Game Studio](/img/production/dashboard/import-game-studio.png)

:::info
If the parent studio **does exist**, it is **not** necessary to first select the studio you want to import to. Elm Story will import and select the parent studio for you.
:::

### Game Exists

If the game you are importing already exists, Elm Story will inform you if the local, internal data is newer and ask you to confirm overwrite.

![Import Game Exists](/img/production/dashboard/import-game-exists.png)

## Validation Errors

As part of import processing, Elm Story validates game data. If your game fails validation, an error modal will display with a list of validation errors.

![Import Error](/img/production/dashboard/import-error.png)

You will also receive an error if you attempt to import game data that Elm Story does not support.

![Import Error Version](/img/production/dashboard/import-error-version.png)

## 3rd Party Tools

The Elm Story game format is open for 3rd party use and development.

Schema is hosted on NPM via the [@elmstorygames/schema](https://www.npmjs.com/package/@elmstorygames/schemas) package.

If you wish for Elm Story Games to create an import/export plugin for your tool, consider [sponsoring](https://www.patreon.com/ElmStoryGames) Elm Story and we will evaluate effort and prioritize (within reason) development for a future release.
