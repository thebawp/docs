---
title: Composer Overview
pagination_label: Composer Overview
pagination_prev: guides/production/dashboard/managing-worlds
sidebar_label: Overview
sidebar_position: 0
slug: overview
---

Clicking a game box in the dashboard will take you to the editor.

:::note
When you are just starting out with Elm Story, the editor can feel a little overwhelming. Read through this bulleted guide quickly from top to bottom, but don't feel as though you need to remember everything. As you get more comfortable with the interface, refer back to validate and reinforce your memory.

If you prefer a natural introduction to Elm Story, consider [joining](https://elmstorygames.eventbrite.com/) our weekly livestream where you can ask questions as we go. The [r/ElmStoryGames](https://reddit.com/r/ElmStoryGames) subreddit is also a great place to ask questions, request clarifications and provide feedback.
:::

![Editor UI](/img/production/editor/editor-ui.png)

:::caution
The Styles and Problems panel are currently disabled.
:::

:::info
Title bar buttons will invert position on macOS to match platform convention.
:::

## 1. Game Outline

The Game Outline panel starts with the title bar.

- The back button will return you to the dashboard.
- When another component is selected, click the game title to select the [world (root)](elements/world-root-element.md) component.
- To [export](exporting-worlds.md) your game as a PWA or JSON file, click the export button.
- Add a [folder](elements/folder-element.md), [scene](elements/scene-element.md) or [event](elements/event-element.md) component to your game by clicking the add component button.

Below the title bar is an ordered, nested list of your game's components.

![Component Editor](/img/production/editor/game-outline.png)

- Open a component for editing by clicking the desired component title in the list.
- Reorder and nest game components via drag and drop.
- Right-click a component to rename, remove or add a nested component.
- Click the caret to the left of the parent component title to expand or collapse.
- Collapsed parent components display children count to the right of the component title.

:::info
The game (root) component supports folder and scene child components.

Scene components support passage child components.
:::

:::info
Both the add component menu and component context (right-click) menu will display supported options, contextual to your current selection.
:::

## 2. Component Editor

The Component Editor provides an expansive view for working on multiple scenes and testing your game.

![Component Editor](/img/production/editor/component-editor.png)

- Selecting a scene will open/focus the respective scene tab.
- Selecting a passage will open/focus the parent scene tab and focus the respective passage node.
- Like Photoshop, organize your layout by dragging and dropping game and scene tabs.
- Below each tab title are contextual tools.

:::info
Open components and custom layouts are **not** currently persisted between sessions.
:::

### Game Tab

Hosts a hot-reloading, embedded version of the Elm Story Runtime Engine (ESRE) used for testing your game.

![Game Tab](/img/production/editor/game-tab.png)

Below the tab title is the devtool bar:

#### Reset

- Restarts game after resetting ESRE's bookmarks, events and state.

:::info
ESRE will recommend a game reset when you make changes the runtime engine is unable to hot-reload.
:::

#### Xray

- Toggles a debug panel displaying current event and game state information.
- Clicking the passage ID will open/focus the parent scene tab and focus the respective passage node.

#### Expressions

- Toggles [expression](expressions.md) highlighting.
- Hover a highlighted expression to peak the underlying code.

#### Blocked Choices

- Toggles display of blocked [choices](elements/choice-element.md).
- Used for debugging [conditions](elements/condition-element.md) and missing [paths](elements/path-element.md).

### Scene Tab

An editable map ([graph](https://en.wikipedia.org/wiki/Graph_theory)) of a scene's passages and [jumps](elements/jump-element.md) ([nodes/vertices](<https://en.wikipedia.org/wiki/Vertex_(graph_theory)>)) and connected routes ([edges](https://en.wikipedia.org/wiki/Glossary_of_graph_theory#edge)).

![Scene Tab](/img/production/editor/scene-tab.png)

Below the tab title is a contextual tool bar:

#### + Passage / + Jump

- Available when nothing is selected.
- Adds passage or jump in the center of the view.

#### Center Selection

- Available when passages, jumps or routes are selected.
- Centers selection, but does not zoom.
- Used when you have a selected passage, jump or route that is not currently in view.

Below the tool bar is the scene map.

#### Scene Map

- Click-drag the background to pan.
- Scroll your mouse wheel to zoom.
- Right-click an empty spot to add passage and jump nodes at click position.
- Click node headers to select node.
- Right-click node headers for additional options, including remove.
- Click-drag node headers to reposition nodes.
- Click-drag node handles to create routes between nodes.
- After selecting a single node, press backspace to remove connected routes.
- Click edges to select routes.
- After selecting a single edge, press backspace to remove selected route.
- Select a passage node to add choices.
- Double-click choice titles to rename inline.
- Right-click choices for additional options, including remove.
- Double-click passage node headers to edit contents.
- Shift-click-drag across nodes to select multiple nodes and routes for reposition.

In the bottom-left corner are map controls for zooming in and out, fitting the view and locking the map from edits.

:::info
Zoom level and position **are** persisted for each game and included during game export.
:::

:::info
Locking the map from edits is **not** currently persisted between sessions.
:::

In the bottom-right corner is the minimap, useful for visualizing your scene layout and what part of your scene map is currently visible.

- Passages are purple.
- Jumps are green.
- Game Over passages are red.

## 3. Component Properties

Selecting a component in the Game Outline or Component Editor will update Component Properties with representative options.

Aside from title, components possess different features, settings and relationships. In the following image, you'll notice that we've selected the `Go East` choice, a child of the `Signpost` passage, which is a child of the `Town Square` scene.

Based on our current selection, Component Properties displays what we can manage in this selected hierarchy.

![Component Properties](/img/production/editor/component-properties.png)

The next guide on [Game Components](elements/world-root-element.md) will cover the properties of each component type in detail.

## 4. Variables

Manage your game's global [variables](elements/variable-element.md).

## 5. About Elm Story

The `Es` button will open the about modal displaying:

- Elm Story version and build numbers.
- External [support](https://patreon.com/ElmStoryGames/join), [community](https://reddit.com/r/ElmStoryGames) and [license](https://elmstory.com/license) links.

![About Elm Story](/img/production/editor/about-elm-story.png)

## 6. Toggle Fullscreen

F11 will also toggle fullscreen mode.
