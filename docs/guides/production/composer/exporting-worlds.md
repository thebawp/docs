---
title: Exporting Storyworlds
sidebar_position: 4
---

:::info
Elm Story currently supports game export to [JSON](https://en.wikipedia.org/wiki/JSON) [(guide)](../../../data/json/overview) and [PWA](https://en.wikipedia.org/wiki/Progressive_web_application) [(guide)](../../../data/pwa/overview).
:::

1. From the [dashboard](../../dashboard/overview), click the game you wish to export.
2. Once the game opens in the [editor](../overview), from the [game outline](../overview/#1-game-outline) title bar, click the export game button.

![Export Game Menu](/img/production/editor/exporting-game-menu.png)

3. From the menu, click your desired format.
   1. **JSON:** Select the folder you wish to export your JSON to, name your file (ending with the `.json` extension) and click save.
   2. **PWA:** Select the folder you wish to export your PWA to and click select or open.

For PWAs, once the export process finishes, Elm Story will open your exported game folder in your file explorer.

![Exported Game As PWA](/img/production/editor/exported-game-as-pwa.png)

Your exported game is ready for [self-hosting](../../../publication/self-hosting).
