---
title: Storyworld (Root) Element
pagination_label: Storyworld (Root) Element
sidebar_position: 0
sidebar_label: World (Root)
---

## Editor

### Component Management

#### Add Game

1. From the dashboard, create or select a studio.
2. Click the add game button.

#### Select Game

1. Click the game's title in the game outline to select.

Alternatively, click the game tab in the component editor to select.

#### Rename Game

1. Select the game.
2. Rename from the component properties panel.

#### Remove Game

1. From the dashboard, in the game box, click the remove storyworld button.
2. Click remove in the confirmation modal.

:::caution
Removing a game will permanently remove all associated components.
:::

### Component Editor

The game component is represented as the game tab in the component editor.

In the game tab, test your game using the bundled, hot-reload supporting Elm Story Runtime Engine (ESRE) and accompanying devtools.

### Component Properties

When the game component is selected, the following properties are editable from the component properties panel:

#### Game Details

- [`title`](#title)

#### Jump On Game Start

- [`jump`](#jump)

#### Metadata

- [`designer`](#designer)
- [`version`](#version)
- [`description`](#description)
- [`copyright`](#copyright)
- [`website`](#website)

## Properties

Game (root) properties make up the game's `_` object.

### `children`

Ordered array of component child references represented as tuples with child component type at index 0 and child component UUID at index 1.

The game component supports unique `FOLDER` and `SCENE` component child references.

#### Property Type

`Array<['FOLDER' | 'SCENE', string]>`

#### JSON Example

```
"children": [
  ["SCENE", "ca122de2-5736-43cc-86fd-5f23c575e7a1"],
  ["FOLDER", "58ee805c-2f58-4614-bfbc-d7d123272517"]
]
```

<hr/>

### `copyright`

The game's copyright information.

#### Property Type

`string`

#### JSON Example

`"copyright": "(c) 2021 Elm Story Games"`

<hr/>

### `description`

A description or summary of the game's content.

#### Property Type

`string`

#### JSON Example

`"description": "You wake in a dusty, frigid and dimly lit room..."`

<hr/>

### `designer`

#### Property Type

`string`

#### JSON Example

`"designer": "ritsuke"`

<hr/>

### `engine`

The Elm Story app and Elm Story Runtime Engine (ESRE) version.

#### Property Type

`string`

#### JSON Example

`"engine": "0.6.0"`

<hr/>

### `id`

Game UUID.

#### Property Type

`string`

#### JSON Example

`"id": "701b0e45-1afa-4755-97d3-b600a52efaf3"`

<hr/>

### `jump`

UUID of [jump](jump-element.md) component used for **Jump On Game Start**.

#### Property Type

`string`

#### JSON Example

`"jump": "0a300279-034a-4b78-88ac-822afec0480c"`

<hr/>

### `schema`

URL of the current engine's hosted [schema](https://json-schema.org/understanding-json-schema/) for use in [validating](https://ajv.js.org/) game data.

#### Property Type

`string`

#### JSON Example

`"schema": "https://elmstory.com/schema/elm-story-0.6.0.json"`

<hr/>

### `studioId`

The game's studio UUID.

#### Property Type

`string`

#### JSON Example

`"studioId": "131415d0-0684-50f6-a474-9797968cd850"`

<hr/>

### `studioTitle`

The game's studio title.

#### Property Type

`string`

#### JSON Example

`"studioTitle": "Elm Story Games"`

<hr/>

### `tags`

#### Property Type

`string[]`

#### JSON Example

`"tags": ["mystery", "thriller", "choice", "parser"]`

<hr/>

### `title`

Game title.

#### Property Type

`string`

#### JSON Example

`"title": "Amber Shores"`

<hr/>

### `updated`

Last time game component changed represented as the milliseconds elapsed since the [UNIX epoch](https://en.wikipedia.org/wiki/Unix_time).

#### Property Type

`number`

#### JSON Example

`"updated": 1636121968781`

<hr/>

### `version`

Game version using [semver](https://semver.org/) format.

#### Property Type

`string`

#### JSON Example

`"version": "0.0.45"`

<hr/>

### `website`

URL of the game's marketing website.

#### Property Type

`string`

#### JSON Example

`"website": "https://elmstory.com"`
