---
title: Folder Element
pagination_label: Folder Element
sidebar_position: 1
sidebar_label: Folder
---

Folders group related scenes and groups (nested folders) of related scenes.

## Editor

### Component Management

#### Add Folder

1. From the game outline, select the game or a folder component.
2. Click the add component button > folder.

Alternatively, right-click a folder component and click add folder to... from the context menu.

#### Select Folder

1. From the game outline, click the desired folder to select.

#### Rename Folder

1. From the game outline, right-click the desired folder and click rename... from the context menu.
2. Press return or click outside the component to save.

Alternatively, select the desired folder and rename from the component properties panel.

#### Remove Folder

1. From the game outline, right-click the desired folder and click remove... from the context menu.

:::caution
Removing a folder will permanently remove all nested folder and scene components.
:::

### ~~Component Editor~~

N/A

### Component Properties

When a folder component is selected, the following properties are editable from the component properties panel:

#### Folder Details

- [`title`](#title)

## Properties

### `children`

Ordered array of component child references represented as tuples with child component type at index 0 and child component UUID at index 1.

The folder component supports unique `FOLDER` and `SCENE` component child references.

#### Property Type

`Array<['FOLDER' | 'SCENE', string]>`

#### JSON Example

```
"children": [
  ["SCENE", "ca122de2-5736-43cc-86fd-5f23c575e7a1"],
  ["FOLDER", "58ee805c-2f58-4614-bfbc-d7d123272517"]
]
```

<hr/>

### `id`

Folder UUID.

#### Property Type

`string`

#### JSON Example

`"id": "58ee805c-2f58-4624-bfbc-c7d123272517"`

<hr/>

### `parent`

Tuple referencing parent component with parent component type at index 0 and parent component ID at index 1.

The folder component supports the `GAME` or a unique `FOLDER` component parent reference.

:::info
If the parent reference is of component type `GAME`, the component ID is null.
:::

#### Property Type

```
['GAME' | 'FOLDER', string | null]
```

#### JSON Example

`"parent": ["GAME", null]`

`"parent": ["FOLDER", "58ee805c-2f58-4624-bfbc-c7d123272517"]`

<hr/>

### `tags`

#### Property Type

`string[]`

#### JSON Example

`"tags": ["city", "market"]`

<hr/>

### `title`

Folder title.

#### Property Type

`string`

#### JSON Example

`"title": "NPCs"`

<hr/>

### `updated`

Last time folder component changed represented as the milliseconds elapsed since the [UNIX epoch](https://en.wikipedia.org/wiki/Unix_time).

#### Property Type

`number`

#### JSON Example

`"updated": 1636121968781`
