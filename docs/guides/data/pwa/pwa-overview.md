---
title: PWA Overview
pagination_label: PWA Overview
sidebar_label: Overview
sidebar_position: 0
slug: overview
---

Elm Story supports export of your game to a fully compiled, installable and offline-capable web app ([PWA](https://en.wikipedia.org/wiki/Progressive_web_application)) ready for [self-hosting](../../../publication/self-hosting).

:::info
If you're unfamiliar with progressive web applications, check out the first part of the following [video](https://youtu.be/sFsRylCQblw) from Fireship.

<div class="video-container">
<iframe src="//youtube.com/embed/sFsRylCQblw" frameborder="0" allowfullscreen></iframe>
</div>
:::

Your exported game is bundled with a customized version of the Elm Story Runtime Engine (ESRE).

## Offline Support

When a player first visits your self-hosted game, ESRE installs and registers a service worker with the player's browser. The worker is responsible for installing ([pre-caching](https://developers.google.com/web/tools/workbox/modules/workbox-precaching)) and detecting revisions to your game's data and assets.

Each time an online player returns to your game, the worker will inform ESRE of any changes to the engine and ESRE will prompt the player to update. During gameplay, in the background, the worker will periodically check for revisions every hour.

If the player's device is offline, your game will continue to work.

## Game Updates

ESRE uses revision feedback from the service worker and compares game versions to determine when a game update is available to the player. For example, if you self-host version 0.0.1 of your game, make changes in Elm Story and re-export and re-upload as version 0.0.2, ESRE will prompt the player to update.

:::caution
If you make any changes to your game, you must [increase](#increase-version) your game's metadata version number.

ESRE will throw a silent error in the player's browser console if it detects an older, incoming version than the game currently installed.
:::

### Increase Version

Game version must conform to the [semantic versioning](https://semver.org/spec/v2.0.0.html) standard.

Your game's version is adjusted in the [composer](../../../production/composer/overview), via the [element properties](../../../production/composer/overview/#3-component-properties) panel.

1. In the editor, click the game title in the game outline title bar or game tab.
2. Under component properties, click the metadata sub panel to expand.
3. Adjust version and press return or click save at the bottom of the form.

![Increase Version](/img/data/pwa/increase-version.png)

Elm Story provides feedback if the supplied format does not conform to the [semver](https://semver.org/spec/v2.0.0.html) standard.

### State Patching

To ensure evolving game design flexibility and retention of player progress during updates, ESRE patches and versions game state. For example, if you remove a passage in a future version that is the player's current event destination in their installation, ESRE will patch game state by rewinding the event timeline until it finds the most recent, existing event destination.

During early access, you may encounter edge cases where game state degrades after updating. Please report issues to our community subreddit, [r/ElmStoryGames](https://reddit.com/r/ElmStoryGames).

## Engine Updates

Each Elm Story release ships with a matching version of ESRE. This means that when you export your game as a PWA, your game is bundled with a version of ESRE that matches your Elm Story installation.

If your game doesn't require changes, but you want your players to enjoy the latest features of the engine, re-export with the most recent version of Elm Story and re-upload to your host.

When players receive the update prompt, changes are limited to ESRE. It is not necessary to increase your game's version.
