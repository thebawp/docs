---
title: Installation
sidebar_position: 1
---

Elm Story is available as a [free download](https://elmstorygames.itch.io/elm-story/purchase) for macOS, Linux and Windows. If you use the Itch.io [app](https://itch.io/app), new versions will automatically download to your library, but you will need to manually install updates.

![Itch.io app](/img/installation/itch-app.png)

Future versions of Elm Story may include an opt-in for automatic update installation.

## Offline Support

Aside from the background download of the spell check dictionary, Elm Story does not use network communications and has full offline support.

:::info
Elm Story does not call home, collect analytics or require account signup.
:::
